Hackernew Top words API
-------------------

+ Dependencies: npm, nodejs


installation
------------

> npm install

There is only a dev command to start the nodejs app

> npm run start:dev

Endpoints
---------

- Getting words from comments of New/Best/Top Stories
> http://localhost:3000/topwords/comments

- Getting words from titles
> http://localhost:3000/topwords/titles

- Getting words from comments of latets Stories
> http://localhost:3000/topwords/latest