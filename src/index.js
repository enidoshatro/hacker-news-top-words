const express = require("express");
const routes = require("./routes/routes").requestHandlers

const app = express();

app.get("/topwords/:type", async (req, res, next) => {
    type = req.params.type
    if (type in routes) {
        return routes[type](req, res)
    }

    res.status(404).json('Not found');
});

app.listen(3000, () => {
    console.log("Server running on port 3000");
});