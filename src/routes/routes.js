const { getCommentsWords, getTitlesWords, getLatestCommentsWords } = require("../models/words").default;

const comments = async (req, res) => {
    try {
        const items = await getCommentsWords()
        res.json(items)
    } catch (error) {
        res.status(500).json('Some error');
        console.error(error)
    }
};
const titles = async (req, res) => {
    try {
        const items = await getTitlesWords()
        res.json(items)
    } catch (error) {
        res.status(500).json('Some error');
        console.error(error)
    }
};

const latest = async (req, res) => {
    try {
        const items = await getLatestCommentsWords()
        res.json(items)
    } catch (error) {
        res.status(500).json('Some error');
        console.error(error)
    }
};

exports.requestHandlers = { comments, titles, latest };