const axios = require("axios").default;
const stopword = require('stopword');

const API_URL = 'https://hacker-news.firebaseio.com/v0';
const topStoriesURL = `${API_URL}/topstories.json`
const newStoriesURL = `${API_URL}/newstories.json`
const bestStoriesURL = `${API_URL}/beststories.json`
const lastestStoryURL = `${API_URL}/maxitem.json`
const getItemURL = storyId => `${API_URL}/item/${storyId}.json`

const getStories = async (storyIds) => {
    const stories = storyIds.map(storyId => axios.get(getItemURL(storyId)))
    return Promise.all(stories)
}

const prepareItems = (items, numberOfItems) => {
    return items.sort((first, second) => {
        return second[1] - first[1];
    }).slice(0, numberOfItems)
}

const getCommentsWords = async () => {
    const chunks = 1000
    const topResults = await axios.get(topStoriesURL)
    const newResults = await axios.get(newStoriesURL)
    const bestResults = await axios.get(bestStoriesURL)
    const storyIds = [...new Set([...topResults.data, ...newResults.data, ...bestResults.data]),]
    const firstStoryIds = storyIds.slice(0, chunks)

    const items = (await getStories(firstStoryIds))
        .map(res => res.data)
        .filter(item => item && item.text)
        .reduce((acc, item) => [...acc, ...stopword.removeStopwords(item.text.split(' '))], [])
        .reduce((acc, item) => {
            if (item in acc) {
                acc[item]++
            } else {
                acc[item] = 1
            }
            return acc
        }, {})

    return prepareItems([...Object.entries(items)], 25)
}

const getLatestCommentsWords = async () => {
    const numberOfItems = 1000
    const latestItemIdResult = await axios.get(lastestStoryURL)
    let latestStoryId = latestItemIdResult.data
    const latestStoryIds = Array.from({ length: numberOfItems }, () => (latestStoryId -= 1))

    const items = (await getStories(latestStoryIds))
        .map(res => res.data)
        .filter(item => item && item.text)
        .reduce((acc, item) => [...acc, ...stopword.removeStopwords(item.text.split(' '))], [])
        .reduce((acc, item) => {
            if (item in acc) {
                acc[item]++
            } else {
                acc[item] = 1
            }
            return acc
        }, {})

    return prepareItems([...Object.entries(items)], 25)
}

const getTitlesWords = async () => {
    const chunks = 250
    const newResults = await axios.get(newStoriesURL)
    const storyIds = [...new Set([...newResults.data]),]
    const firstStoryIds = storyIds.slice(0, chunks)

    const items = (await getStories(firstStoryIds))
        .map(res => res.data)
        .filter(item => item && item.title)
        .reduce((acc, item) => [...acc, ...stopword.removeStopwords(item.title.split(' '))], [])
        .reduce((acc, item) => {
            if (item in acc) {
                acc[item]++
            } else {
                acc[item] = 1
            }
            return acc
        }, {})

    return prepareItems([...Object.entries(items).concat()], 25)
}

exports.default = { getCommentsWords, getTitlesWords, getLatestCommentsWords };